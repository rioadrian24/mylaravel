@extends('layout.master')

@section('title', 'Home Page')

@section('content')
@if (session('success')) {!! session('success') !!} @endif
@if (session('error')) {!! session('error') !!} @endif
<div class="row justify-content-center">
	<div class="col-lg-10">
		<div class="card shadow mt-5 animated flipInX">
			<div class="card-header">
				<h5 class="text-info float-left"><i class="fas fa-user"></i> Profile Anda</h5>
				<a href="/logout" class="btn btn-danger btn-sm float-right">Logout <i class="fas fa-sign-out-alt"></i></a>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-3 border-right">
						<img src="{{ $biodata->getImage() }}" class="img-fluid img-thumbnail rounded-circle shadow overflow-hidden">
						@if (auth()->user()->status > 0)
						<a href="#" class="btn btn-info mt-3 w-100 btn-sm">Edit Profile <i class="fas fa-edit"></i></a>
						@endif
					</div>
					<div class="col-lg-9">
						@if (auth()->user()->status == 0)
						<div class="animated bounceInDown slow">
								<h5 class="text-muted mt-3 text-center">Selamat Datang {{ auth()->user()->nama }}</h5>
								<h6 class="text-muted text-center">Silahkan lengkapi profil anda terlebih dahulu.</h6>
						</div>
						<div class="row justify-content-center mt-3">
							<a href="/profile/{{ auth()->user()->id }}/edit" class="btn btn-outline-success btn-sm mx-1 shadow animated fadeInDown slow">Lengkapi Profile <i class="fas fa-edit"></i></a>
							<a href="/logout" class="btn btn-outline-danger btn-sm mx-1 shadow animated fadeInDown slow">Logout <i class="fas fa-sign-out-alt"></i></a>
						</div>
						@else
						<h5>Nama : <span class="text-muted">{{ $biodata->user->nama }}</span></h5>
						<h5>Email : <span class="text-muted">{{ $biodata->user->email }}</span></h5>
						<h5>NIM : <span class="text-muted">{{ $biodata->nim }}</span></h5>
						<h5>TTL : <span class="text-muted">{{ $biodata->ttl }}</span></h5>
						<h5>Jenis Kelamin : <span class="text-muted">
							@if ($biodata->jk == 'L')
							Laki-Laki
							@else
							Perempuan
							@endif
						</span></h5>
						<h5>Alamat : <span class="text-muted">{{ $biodata->alamat }}</span></h5>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop