<!DOCTYPE html>
<html lang="en">
<head>
	<title>@yield('title')</title>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="Content-Language" content="en">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
	<link rel="stylesheet" href="{{ asset('vendor/') }}assets/datatables/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" href="{{ asset('assets/font-awesome/css/all.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.mod.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/animated/animated.css') }}">
	<script src="{{ asset('assets/sweet-alert2/sweetalert2.all.min.js') }}"></script>
</head>
<body>
	
	<div class="container">
		@yield('content')
	</div>

	<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
	<script src="{{ asset('assets/js/popper.min.js') }}"></script>
	<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('assets/datatables/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('assets/datatables/dataTables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('assets/datatables/datatables-demo.js') }}"></script>
</body>
</html>