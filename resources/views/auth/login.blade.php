@extends('layout.master')

@section('title', 'Login Page')

@section('content')
@if (session('success')) {!! session('success') !!} @endif
<div class="row justify-content-center">
	<div class="col-lg-6">
		<div class="card shadow mt-5">
			<div class="card-header">
				<h5 class="text-info">Login <i class="fas fa-sign-in-alt"></i></h5>
			</div>
			<div class="card-body">
				<form action="/postlogin" method="post">
					{{ @csrf_field() }}
					<div class="form-group">
						<label>Email</label>
						<input type="text" class="form-control" name="email" placeholder="Masukkan Email">
					</div>
					<div class="form-group">
						<label>Password</label>
						<input type="password" class="form-control" name="password" placeholder="Masukkan Password">
					</div>
					<p>
						Tidak memiliki akun ? <a href="/register">Register</a>
					</p>
					<button type="submit" class="btn btn-info btn-sm float-right">Login <i class="fas fa-check"></i></button>
				</form>
			</div>
		</div>
	</div>
</div>
@stop