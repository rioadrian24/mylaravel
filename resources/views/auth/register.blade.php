@extends('layout.master')

@section('title', 'Register Page')

@section('content')
<div class="row justify-content-center">
	<div class="col-lg-6">
		<div class="card shadow mt-5">
			<div class="card-header">
				<h5 class="text-info">Register <i class="fas fa-user-plus"></i></h5>
			</div>
			<div class="card-body">
				<form action="/postregister" method="post">
					{{ @csrf_field() }}
					<div class="form-group">
						<label>Nama</label>
						<input type="text" class="form-control" name="nama" placeholder="Masukkan Nama" value="{{ old('nama') }}">
						@if ($errors->has('nama'))
							<span class="text-danger">{{ $errors->first('nama') }}</span>
						@endif
					</div>
					<div class="form-group">
						<label>Email</label>
						<input type="text" class="form-control" name="email" placeholder="Masukkan Email" value="{{ old('email') }}">
						@if ($errors->has('email'))
							<span class="text-danger">{{ $errors->first('email') }}</span>
						@endif
					</div>
					<div class="form-group">
						<label>Password</label>
						<input type="password" class="form-control" name="password" placeholder="Masukkan Password">
						@if ($errors->has('password'))
							<span class="text-danger">{{ $errors->first('password') }}</span>
						@endif
					</div>
					<div class="form-group">
						<label>Confirm Password</label>
						<input type="password" class="form-control" name="re-password" placeholder="Ulangi Password">
					</div>
					<p>
						Sudah memiliki akun ? <a href="/login">Login</a>
					</p>
					<button type="submit" class="btn btn-info btn-sm float-right">Register <i class="fas fa-check"></i></button>
				</form>
			</div>
		</div>
	</div>
</div>
@stop