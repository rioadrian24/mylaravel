<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AuthController@index')->name('login');
Route::get('/login', 'AuthController@index')->name('login');
Route::post('/postlogin', 'AuthController@postlogin');
Route::get('/register', 'AuthController@register');
Route::post('/postregister', 'AuthController@postregister');
Route::get('/logout', 'AuthController@logout');

Route::group(['middleware' => 'auth'], function() {
    Route::get('/home', 'HomeController@index');
    Route::get('/profile', 'HomeController@profile');
    Route::get('/profile/{id}/edit', 'HomeController@edit');
    Route::post('/profile/{id}/update', 'HomeController@update');
});