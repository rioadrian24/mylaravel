<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Biodata;
use Auth;

class AuthController extends Controller
{
    public function index()
    {
    	return view('auth.login');
    }

    public function postlogin(Request $request)
    {
    	if (Auth::attempt($request->only('email','password'))) {
    		return redirect('/home');
    	} else {
    		return redirect('/login');
    	}
    }

    public function register()
    {
    	return view('auth.register');
    }

    public function postregister(Request $request)
    {
    	$this->validate($request,[
    		'nama' => 'required',
    		'email' => 'required|email|unique:users',
    		'password' => 'required|same:re-password|min:6'
    	]);

		// $user = User::create($request->all());
		
		$user_id = random_int(10000000, 100000000);

		User::create([
			'id' => $user_id,
			'nama' => $request->nama,
			'email' => $request->email,
			'password' => bcrypt($request->password),
			'status' => 0
		]);

		Biodata::create(['user_id' => $user_id]);

		return redirect('/login')->with('success', '<script>swal.fire({title: "success",text: "Registrasi berhasil, Silahkan login!",type: "success"});</script>');
    }

    public function logout()
    {
    	Auth::logout();
    	return redirect('/login')->with('success', '<script>swal.fire({title: "success",text: "Anda telah logout!",type: "success"});</script>');
    }
}
