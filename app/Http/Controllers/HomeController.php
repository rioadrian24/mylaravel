<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Biodata;

class HomeController extends Controller
{
	public function index()
	{
		$user_id = auth()->user()->id;
		$biodata = Biodata::where('user_id',$user_id)->first();

		return view('home.index', ['biodata' => $biodata]);
	}

	public function edit(Request $request, $id)
	{
		$user = User::find($id);
		return view('home.profile');
	}

	public function update(Request $request, $id)
	{
		$this->validate($request,[
			'nim' => 'required|unique:biodata',
			'ttl' => 'required',
			'jk' => 'required',
			'alamat' => 'required',
		]);

		$biodata = Biodata::where('user_id', $request->user_id)->first();
		if ($biodata->nim == null) {
			DB::table('biodata')->where('user_id', $request->user_id)->update([
				'nim' => $request->nim,
				'ttl' => $request->ttl,
				'jk' => $request->jk,
				'alamat' => $request->alamat
			]);
			User::find($id)->update(['status' => 1]);
			return redirect('/home')->with('success', '<script>swal.fire({title: "Success",text: "Anda berhasil melengkapi profile, Terimakasih!",type: "success"});</script>');
		} else {
			return redirect('/profile/'.$request->user_id.'/edit')->with('error', '<script>swal.fire({title: "Warning",text: "Anda telah melengkapi profile!",type: "info"});</script>');
		}
	}
}