<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Biodata extends Model
{
    protected $table = 'biodata',
    		  $guarded = ['nama','email'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function getImage()
    {
        if ($this->gambar == null) {
            return asset('assets/img/users/default.png');
        } else {
            return asset('assets/img/users/' . $this->gambar); 
        }
    }
}
